import {DomListener} from '@core/DomListener'

export class ExcelComponent extends DomListener {
    constructor($root, options = {}) {
        super($root, options.listeners);
        this.name = options.name || ''
        this.emitter = options.emitter
        this.unsubcribers = []
        this.prepare()
    }
    // Настраиваем наш компонент до init
    prepare() {}
    // Возвращаем шаблон компонента
    toHTML() {
        return ''
    }
    // Уведомляем слушателя про событие event
    $emit(event, ...args) {
        this.emitter.emit(event, ...args)
    }
    // Подписываемся на событие event
    $on(event, fn) {
        const unsub = this.emitter.subscribe(event, fn)
        this.unsubcribers.push(unsub)
    }
    // Иницилизируем компонент
    // Добавляем DOM слушателей
    init() {
        this.initDomListener()
    }
    // Удаляем компонент
    // Чистим слушателей
    destroy() {
        this.removeDomListener()
        this.unsubcribers.forEach(unsub => unsub())
    }
}
